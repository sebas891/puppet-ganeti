# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.box = "debian-10-amd64"

  config.vm.provider :libvirt do |libvirt|
    libvirt.cpus = 2
    libvirt.memory = 2048
    libvirt.nested = true
    # Additional disk for storage LVM. '21G' is used since '20G' evaluates to
    # slightly less space than the minimum for cluster initialization.
    libvirt.storage :file, :size => '21G', :path => 'ganeti-data.img',
                    :allow_existing => true
  end

  config.vm.define :test do |test|
    test.vm.hostname = 'ganeti.local'

    # Do not autoconfigure this interface, we will use it to make the bridge
    # Ganeti needs a bridge to attach the network since it will connect
    # instance interfaces to this bridge to provide networking to the
    # instances. but vagrant doesn't offer automation to configure this so we
    # have to configure it manually
    test.vm.network :private_network, :ip => "10.0.0.2", :auto_config => false
    test.vm.provision "configure bridge interface", type: "shell", run: "once" do |s|
      s.inline = <<-SHELL
        if ! ip link show br0 >/dev/null 2>/dev/null; then
          echo "Configuring and bringing up private bridge"
          ip link add br0 type bridge
        fi
        if ! ip link show master br0 2>/dev/null | grep -q eth1; then
          echo "Attaching eth1 to bridge br0"
          ip link set eth1 master br0
          ip a add 10.0.0.2/24 dev eth1
          ip link set eth1 up
        fi
      SHELL
    end

    test.vm.provision "configure ganeti data lvm", type: "shell", run: "once" do |s|
      s.inline = <<-SHELL
        apt-get install -y lvm2
        pvs | grep /dev/vdb > /dev/null 2> /dev/null
        R="$?"
        if [ "$R" -eq 1 ] && [ -b /dev/vdb ]; then
           pvcreate /dev/vdb
           vgcreate data /dev/vdb
        fi
        SHELL
    end

    test.vm.provision "configure hosts file", type: "shell", run: "once" do |s|
      s.inline = <<-SHELL
        echo "10.0.0.2 ganeti.local # This node
10.0.0.3 ganeti-cluster.local # The cluster IP
" >> /etc/hosts
        # Ganeti doesn't like having the current node on a loopback
        sed -i '/127\.0\.1\.1/d' /etc/hosts
      SHELL
    end

    # You do NOT want to have librarian-puppet work on a synced_dir if it's
    # using nfs: this could destroy all your files on the host.
    #
    # The Puppetfile in tests/ currently tells librarian-puppet to clone the
    # master branch, so when running `vagrant provision`, only commits on top
    # of that branch will get applied. You can change the branch name in the
    # Puppetfile to provision using another branch.
    test.vm.provision "install librarian-puppet and install dependencies", type: "shell" do |s|
      s.inline = <<-SHELL
        apt-get update
        apt-get install -y librarian-puppet git augeas-tools
        cd /tmp/vagrant-puppet/
        [[ -L Puppetfile ]] || ln -s /vagrant/tests/Puppetfile Puppetfile
        librarian-puppet install --verbose
      SHELL
    end

    test.vm.provision :puppet do |puppet|
      puppet.manifests_path = "tests"
      puppet.manifest_file = "install.pp"
      puppet.options = ["--modulepath", "/tmp/vagrant-puppet/modules"]
    end

    # Hopefully the puppet module has done its job at this point. This script
    # will make it possible to start testing ganeti right away after
    # provisioning.
    test.vm.provision "initialize ganeti cluster (if not already done)", type: "shell" do |s|
      s.inline = <<-SHELL
        if [ -x /usr/sbin/gnt-cluster ] && [ -x /usr/sbin/gnt-network ]; then
          if ! /usr/sbin/gnt-cluster getmaster >/dev/null 2>/dev/null; then
            echo "Initializing ganeti cluster"
            /usr/sbin/gnt-cluster init --enabled-hypervisor=kvm --master-netdev=br0 --vg-name=data -N link=br0 -H kvm:kernel_path="",initrd_path="" ganeti-cluster.local
            echo "Creating an connecting cluster network"
            /usr/sbin/gnt-network add --network 10.0.0.0/24 --gateway 10.0.0.1 vagrant
            /usr/sbin/gnt-network connect -N link=br0 vagrant default
            # Instances should not use the node's IP
            /usr/sbin/gnt-network modify --add-externally-reserved-ips=10.0.0.2 vagrant
          fi
        fi
      SHELL
    end

    # With this, we can re-run provision without intervention in the VM once
    # we've made a new commit in the code repository.
    test.vm.provision "purge librarian caches", type: "shell" do |s|
      s.inline = <<-SHELL
        rm -rf /tmp/vagrant-puppet/Puppetfile.lock /tmp/vagrant-puppet/.tmp/librarian/cache/source/git /tmp/vagrant-puppet/modules/ganeti
      SHELL
    end
  end
end
