# ganeti::service
#
# @summary Make sure that the ganeti services are running.
#
# @api private
#
class ganeti::service {

  assert_private()

  service { 'ganeti':
    ensure => running,
  }

}
