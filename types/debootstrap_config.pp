# @summary Default configurations for ganeti-instance-debootstrap
#
# These values will be used in /etc/default/ganeti-instance-debootstrap
#
type Ganeti::Debootstrap_config = Struct[{
  Optional[proxy]               => String[1],
  Optional[mirror]              => String[1],
  Optional[arch]                => String[1],
  Optional[suite]               => String[1],
  Optional[extra_packages]      => Array[String[1]],
  Optional[components]          => Array[String[1]],
  Optional[customize_dir]       => Stdlib::Absolutepath,
  Optional[generate_cache]      => Enum['yes', 'no'],
  Optional[clean_cache]         => Integer[1],
  Optional[partition_style]     => Enum['none','msdos'],
  Optional[partition_alignment] => String[1],
}]
